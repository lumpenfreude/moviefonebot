import os
import facebook 
from datetime import datetime
import time
import discord
from dotenv import load_dotenv

# loads environmental variables so you don't have to store raw data in this fucker
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
FTOKEN = os.getenv('FACEBOOK_TOKEN')
GROUP = os.getenv('FACEBOOK_GROUPID')

# this is the graph request that gets shit from facebook. 
graphreq = GROUP+'?fields=events{name,start_time,id,timezone,description,cover,place,attending_count,maybe_count}'

client = discord.Client()

@client.event
async def on_ready():
    print(f'{client.user} is online.')

@client.event
async def on_message(message):
    # this just ensures that the bot doesn't respond to itself
    if message.author == client.user:
        return
    # look at message. if message starts with moviefone, chop it up into a dict of words divided up based on spaces. 
    # then, look at the first word after moviefone (args[1], args start with 0 dumbass) and do some if/elif/else shit with that 
    if message.content.startswith("moviefone"):
        args = message.content.split(' ')
        cmd = args[1]
        # this is where we're actually talking to facebook 
        graph = facebook.GraphAPI(access_token=FTOKEN, version='2.12')
        events = graph.request(graphreq)
        eventEvents = events['events']
        # eventData is a huge fucking dict of all of the events that have ever occured in whatever group ID you've put in your env
        eventData = eventEvents['data']

        if cmd == 'future':
            # since eventData is ALL the events, let's just get info for 15 events
            # you might wanna increase this if you have a ton of events or decrease it if you have less iunno
            for gw in range(0,14):
                eventName = eventData[gw]['name']
                eventID = eventData[gw]['id']
                datetimeStr = eventData[gw]['start_time']
                # the next two lines use datetime.strptime to convert facebook's dumb graph time format to UTC code
                # facebook's graph time is literally UTC time except with a T instead of a space at one point. 
                # WHY ON EARTH WOULD FACEBOOK DO THAT IT BREAKS EVERYTHING SO I HAVE TO DO THIS DUMB SHIT
                # ahem. anyway then stftime converts the datetimeObj into something that is easier on the eyes. 
                # and converting the event date to an actual datetime object is important -- see line 57
                datetimeObj = datetime.strptime(datetimeStr, '%Y-%m-%dT%H:%M:%S%z')
                formalTime = datetimeObj.strftime('%a, %b %d at %-I:%M %p')
                # use the location in the dict as an identifier for the end user, but add a 1 to it because sets start with 0 and
                # ...well let's be real that's ugly
                msg = "# "+str(gw+1)+" "+"*"+eventName+"*"
                # compare timestamp of event to timestamp of The Now, if it's in the future, print it up as an embed
                # this is why you need to convert to a fkn datetime object, so you can compare. 
                if ( datetime.timestamp(datetimeObj) > time.time() ):
                    # really simple embed code with a link to the event, its name, the date, and a number for more info.
                    embed = discord.Embed(title=msg, description=formalTime, url="https://www.facebook.com/events/"+eventID)
                    await message.channel.send(embed=embed)
        
        # everything in this is literally exactly the same as above except > is < lmao 
        # also this isn't gonna show 15 past objects, it'll show 15 minus however many future objects there are
        # oh also there is almost no risk of there being a "present object" since ur converting to unix/epoch time so it's down to the millisecond
        elif cmd == 'past':
            for gw in range(0,14):
                eventName = eventData[gw]['name']
                eventID = eventData[gw]['id']
                datetimeStr = eventData[gw]['start_time']
                datetimeObj = datetime.strptime(datetimeStr, '%Y-%m-%dT%H:%M:%S%z')
                formalTime = datetimeObj.strftime('%a, %b %d at %-I:%M %p')
                msg = "# "+str(gw+1)+" "+"*"+eventName+"*"
                if ( datetime.timestamp(datetimeObj) < time.time() ):
                    embed = discord.Embed(title=msg, description=formalTime, url="https://www.facebook.com/events/"+eventID)
                    await message.channel.send(embed=embed)
        
        # really basic help command, iunno i should maybe jazz this up with an embed, certainly if i add more to this
        elif cmd == 'help':
            await message.channel.send("Type **moviefone future** for a list of upcoming events, and **moviefone** and the number listed to get more information about that event")
        
        else:
            try:
                # try to convert the string from the input to an integer. if it's a number, this will go well. if it's not, it will error.
                # if it is an integer, subtract 1 because we've added 1 to the display for aesthetic purpose 
                num = int(cmd)-1
            except:
                # if the response can't be turned into an integer and isn't one of the above words, spit out an error.
                await message.channel.send("`DOES NOT COMPUTE`")
            else:
                # assign all the various dict data to variables or whatever, u know based on our new int
                eventName = eventData[num]['name']
                eventID = eventData[num]['id']
                datetimeStr = eventData[num]['start_time']
                eventTZ = eventData[num]['timezone']
                eventDesc = eventData[num]['description']
                eventImg = eventData[num]['cover']['source']
                eventPlace = eventData[num]['place']['name']
                eventYes = eventData[num]['attending_count']
                eventMaybe = eventData[num]['maybe_count']
                
                # explained above, this time the time is fancier though... ooh
                datetimeObj = datetime.strptime(datetimeStr, '%Y-%m-%dT%H:%M:%S%z')
                formalTime = datetimeObj.strftime('%a, %b %d at %-I:%M %p')
                
                # tie attendance ints into a snarky string about how ppl never show up to events 
                eventAttendance = str(eventYes)+" definitely going, "+str(eventMaybe)+" say they might... but we all know what that means."
                
                # embed code, this time with an image, and some nice formatting. 
                embed=discord.Embed(title=eventName, url="https://www.facebook.com/event"+eventID, description=formalTime+" "+eventTZ+"\n\n"+eventDesc, color=0xff00ff)
                embed.set_author(name=eventPlace+" presents", url="http://"+eventPlace)
                embed.set_image(url=eventImg)
                embed.set_footer(text=eventAttendance)
                await message.channel.send(embed=embed)
        
# ok now let's run this garbage        
client.run(TOKEN)

